<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommentaireRepository")
 */
class Commentaire
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\produit", inversedBy="commentaires")
     */
    private $id_produit;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $textcomment;

    /**
     * @ORM\Column(type="date")
     */
    private $datecomment;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\user", inversedBy="commentaires")
     */

    private $product;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\user")
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\produit")
     * @ORM\JoinColumn(nullable=false)
     */
    private $idd_produit;



    public function __construct()
    {
        $this->id_produit = new ArrayCollection();

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|produit[]
     */
    public function getIdProduit(): Collection
    {
        return $this->id_produit;
    }

    public function addIdProduit(produit $idProduit): self
    {
        if (!$this->id_produit->contains($idProduit)) {
            $this->id_produit[] = $idProduit;
        }

        return $this;
    }

    public function removeIdProduit(produit $idProduit): self
    {
        if ($this->id_produit->contains($idProduit)) {
            $this->id_produit->removeElement($idProduit);
        }

        return $this;
    }

    public function getTextcomment(): ?string
    {
        return $this->textcomment;
    }

    public function setTextcomment(string $textcomment): self
    {
        $this->textcomment = $textcomment;

        return $this;
    }

    public function getDatecomment(): ?\DateTimeInterface
    {
        return $this->datecomment;
    }

    public function setDatecomment(\DateTimeInterface $datecomment): self
    {
        $this->datecomment = $datecomment;

        return $this;
    }



    public function getProduct(): ?int
    {
        return $this->product;
    }

    public function setProduct(int $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getIdUser(): ?user
    {
        return $this->id_user;
    }

    public function setIdUser(?user $id_user): self
    {
        $this->id_user = $id_user;

        return $this;
    }

    public function getIddProduit(): ?produit
    {
        return $this->idd_produit;
    }

    public function setIddProduit(?produit $idd_produit): self
    {
        $this->idd_produit = $idd_produit;

        return $this;
    }


}
