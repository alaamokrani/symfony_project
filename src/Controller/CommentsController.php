<?php

namespace App\Controller;


use App\Entity\Commentaire;
use App\Entity\Produit;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
class CommentsController extends AbstractController
{
    /**
     * @Route("/comments/{id}", name="comments")
     */
    public function index($id)
    {

        $repository = $this->getDoctrine()->getRepository(Commentaire::class);


        $Avis = $repository->findBy(
            ['idd_produit' => $id]
        );


        $repository = $this->getDoctrine()->getRepository(Produit::class);


        $produit = $repository->find($id);


        return $this->render('produit/comments.twig', [
            'Avis' => $Avis,
            'produit' => $produit
        ]);
    }







    /**
     * @Route("/addcomment/{id}", name="addcomment")
     */
    public function nouveauproduit(Request $request,$id)
    {

        $entityManager = $this->getDoctrine()->getManager();
        $commentaire = new Commentaire();


        $repository = $this->getDoctrine()->getRepository(Produit::class);


        $product = $repository->find($id);



        $comment = $request->request->get('comment');


        $date = new \DateTime();
        $user = $this->getUser();


        $commentaire->setTextcomment($comment);
        $commentaire->setIdUser($user);
        $commentaire->setIddProduit($product);
        $commentaire->setDatecomment($date);

        $entityManager->persist($commentaire);


        $entityManager->flush();






//        dump($product );
//        die();
        return $this->redirectToRoute('comments', [
            'id' => $id
        ]);
    }




}
